Creating a Gentoo installer

When I created this I was the only intended user, and this scope has slipped recently. This script is not supposed to be run without understanding. Use this to understand, and develop your own! Don't miss out on this learning opportunity because you just run and forget!

# Why this script?
The reason I created this script is that Gentoo is my preferred OS and I install it on all of my devices. That is 3 physical devices, and 6 virtual machines(currently; on the rise).
This does not include the amount of times I installed to either experiment or because I failed. I am probably running on 10-15 manual Gentoo installs, so it made sense time wise for me to do this.
I also enjoyed making this script because it was a learning opportunity! I would highly encourage you to attempt this on your own!

# Notes
Google shell coding style

* https://google.github.io/styleguide/shell.xml

Shell Expansion Order

* http://tool.oschina.net/uploads/apidocs/bash-manual/Shell-Expansions.html

Gentoo Installation HandBook

* https://wiki.gentoo.org/wiki/Handbook:AMD64

Other Notes:

`set -o errexit`
Exit  immediately  if a pipeline (which may consist of a single simple command), a list, or a compound command exits with a non-zero status.

`set -o nounset`
Treat unset variables and parameters other than the special parameters "@" and "*" as an error when performing parameter expansion.

`set -o pipefail`
If set, the return value of a pipeline is the value of the last (rightmost) command  to  exit  with  a non-zero  status,  or zero if all commands in the pipeline exit successfully.

---

```bash
#UEFI Firmware partition layout is:
#   Device    Purpose  FileSystem
# /dev/sda1 = Boot Partition vfat
# /dev/sda2 = Swap Partition Swap
# /dev/sda3 = Root Partition ext4
# BIOS Firmware partition layout is:
#   Device    Purpose  FileSystem
# /dev/sda1 = BIOS GRUB Part
# /dev/sda2 = Boot Partition ext2
# /dev/sda3 = Swap Partition Swap
# /dev/sda4 = Root Partition ext4
```


